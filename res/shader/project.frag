#version 330
in vec2 texCoord;
out vec4 outColor;
uniform sampler2D texture;
uniform int textureMode;

void main() {
	if(textureMode == 0)
		outColor = texture2D(texture, texCoord);
	else
		outColor = vec4(texCoord.x,  1 - texCoord.y, 0.0, 1.0);
} 