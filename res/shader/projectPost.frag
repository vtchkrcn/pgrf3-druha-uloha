#version 330
in vec2 texCoord;
out vec4 outColor;
uniform sampler2D texture;

void main()  {

	// NO ANTI-ALIASING
	
	vec3 color = texture2D(texture, texCoord).rgb;
	outColor = vec4(color,1.0);
} 
