package project;


import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import oglutils.OGLBuffers;
import oglutils.OGLRenderTarget;
import oglutils.OGLTextRenderer;
import oglutils.OGLTexture2D;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4PerspRH;
import transforms.Mat4Scale;
import transforms.Vec3D;

/**
 * GLSL sample:<br/>
 * Use render to texture to perform rendering post-processing<br/>
 * Requires JOGL 2.3.0 or newer
 * 
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-11-24
 */

public class Renderer implements GLEventListener, MouseListener,
		MouseMotionListener, KeyListener {

	int width, height, ox, oy;

	OGLBuffers buffers, bufferStrip;
	OGLTextRenderer textRenderer;

	int shaderProgram, FXAAshaderProgramPost, shaderProgramPost, locMat, locTextureMode;
	
	int mode = 0, textureMode = 0;

	OGLTexture2D texture;
	
	Camera cam = new Camera();
	Mat4 proj;

	OGLRenderTarget renderTarget;
	OGLTexture2D.Viewer textureViewer;
	
	@Override
	public void init(GLAutoDrawable glDrawable) {
		// check whether shaders are supported
		OGLUtils.shaderCheck(glDrawable.getGL());
		
		glDrawable.setGL(OGLUtils.getDebugGL(glDrawable.getGL()));
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();

		OGLUtils.printOGLparameters(gl);

		textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());
		
		shaderProgram = ShaderUtils.loadProgram(gl, "/shader/project");
		shaderProgramPost = ShaderUtils.loadProgram(gl, "/shader/projectPost");
		FXAAshaderProgramPost = ShaderUtils.loadProgram(gl, "/shader/FXAAProjectPost");

		createBuffers(gl);
		
		locMat = gl.glGetUniformLocation(shaderProgram, "mat");
		locTextureMode = gl.glGetUniformLocation(shaderProgram, "textureMode");
		
		texture = new OGLTexture2D(gl, "/textures/bricks.jpg");
		
		renderTarget = new OGLRenderTarget(gl, 512, 512);
		
		cam = cam.withPosition(new Vec3D(5, 5, 2.5))
				.withAzimuth(Math.PI * 1.25)
				.withZenith(Math.PI * -0.125);

		gl.glEnable(GL2GL3.GL_DEPTH_TEST);
		textureViewer = new OGLTexture2D.Viewer(gl);
	}

	void createBuffers(GL2GL3 gl) {
		float[] cube = {
					// bottom (z-) face
					1, 0, 0,	0, 0, -1, 	1, 0,
					0, 0, 0,	0, 0, -1,	0, 0, 
					1, 1, 0,	0, 0, -1,	1, 1, 
					0, 1, 0,	0, 0, -1,	0, 1, 
					// top (z+) face
					1, 0, 1,	0, 0, 1,	1, 0, 
					0, 0, 1,	0, 0, 1,	0, 0, 
					1, 1, 1,	0, 0, 1,	1, 1,
					0, 1, 1,	0, 0, 1,	0, 1,
					// x+ face
					1, 1, 0,	1, 0, 0,	1, 0,
					1, 0, 0,	1, 0, 0,	0, 0, 
					1, 1, 1,	1, 0, 0,	1, 1,
					1, 0, 1,	1, 0, 0,	0, 1,
					// x- face
					0, 1, 0,	-1, 0, 0,	1, 0,
					0, 0, 0,	-1, 0, 0,	0, 0, 
					0, 1, 1,	-1, 0, 0,	1, 1,
					0, 0, 1,	-1, 0, 0,	0, 1,
					// y+ face
					1, 1, 0,	0, 1, 0,	1, 0,
					0, 1, 0,	0, 1, 0,	0, 0, 
					1, 1, 1,	0, 1, 0,	1, 1,
					0, 1, 1,	0, 1, 0,	0, 1,
					// y- face
					1, 0, 0,	0, -1, 0,	1, 0,
					0, 0, 0,	0, -1, 0,	0, 0, 
					1, 0, 1,	0, -1, 0,	1, 1,
					0, 0, 1,	0, -1, 0,	0, 1
		};

		int[] indexBufferData = new int[36];
		for (int i = 0; i<6; i++){
			indexBufferData[i*6] = i*4;
			indexBufferData[i*6 + 1] = i*4 + 1;
			indexBufferData[i*6 + 2] = i*4 + 2;
			indexBufferData[i*6 + 3] = i*4 + 1;
			indexBufferData[i*6 + 4] = i*4 + 2;
			indexBufferData[i*6 + 5] = i*4 + 3;
		}
		
		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 3),
				new OGLBuffers.Attrib("inNormal", 3),
				new OGLBuffers.Attrib("inTextureCoordinates", 2)
		};
		
		buffers = new OGLBuffers(gl, cube, attributes, indexBufferData);
		
		// full-screen quad, just NDC positions are needed, texturing
		// coordinates can be calculated from them
		float[] triangleStrip = { 1, -1, 
						1, 1, 
						-1, -1, 
						-1, 1 };
		
		OGLBuffers.Attrib[] attributesStrip = {
				new OGLBuffers.Attrib("inPosition", 2)};

		bufferStrip = new OGLBuffers(gl, triangleStrip, attributesStrip, null);
	}

	@Override
	public void display(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();

		gl.glUseProgram(shaderProgram);

		// set our render target (texture)
		renderTarget.bind();
		
		gl.glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
		gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

		texture.bind(shaderProgram, "texture", 0);
		
		gl.glUniformMatrix4fv(
				locMat,
				1,
				false,
				ToFloatArray.convert(cam.getViewMatrix().mul(proj)
						.mul(new Mat4Scale((double) width / height, 1, 1))), 0);
		gl.glUniform1i(locTextureMode, textureMode);
		
		buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);
		
		
		// POST PROCESSING
		
		int postShader = FXAAshaderProgramPost;
		if(mode == 1) postShader = shaderProgramPost;
		
		gl.glUseProgram(postShader);
		
		// set the default render target (screen)
		gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0);
		
		// reset viewport but fit the aspect ratio of our render target (1:1)
		// best into the window
		if ((double)width/height > 1.0)
			gl.glViewport(0, 0, height, height);
		else
			gl.glViewport(0, 0, width, width);

		gl.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

		// use the result of the previous draw as a texture for the next
		renderTarget.getColorTexture().bind(postShader, "texture", 0);
		
		// draw the full-screen quad
		bufferStrip.draw(GL2GL3.GL_TRIANGLE_STRIP, postShader);

		textureViewer.view(renderTarget.getColorTexture(), -1, -1, 0.6, 1);
		
		String text = new String("FXAA: [LMB] camera, WSAD, [1] FXAA [2] no AA [3] texture on / off");
		
		String currentModeText = new String("FXAA");
		if(mode == 1) currentModeText = new String("no AA");
		
		textRenderer.drawStr2D(3, height-20, text);
		textRenderer.drawStr2D(width - 80, 10, currentModeText);
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width,
			int height) {
		this.width = width;
		this.height = height;
		proj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 1.0, 100.0);
		textRenderer.updateSize(width, height);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		ox = e.getX();
		oy = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		cam = cam.addAzimuth((double) Math.PI * (ox - e.getX()) / width)
				.addZenith((double) Math.PI * (e.getY() - oy) / width);
		ox = e.getX();
		oy = e.getY();
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
			cam = cam.forward(1);
			break;
		case KeyEvent.VK_D:
			cam = cam.right(1);
			break;
		case KeyEvent.VK_S:
			cam = cam.backward(1);
			break;
		case KeyEvent.VK_A:
			cam = cam.left(1);
			break;
		case KeyEvent.VK_CONTROL:
			cam = cam.down(1);
			break;
		case KeyEvent.VK_SHIFT:
			cam = cam.up(1);
			break;
		case KeyEvent.VK_SPACE:
			cam = cam.withFirstPerson(!cam.getFirstPerson());
			break;
		case KeyEvent.VK_R:
			cam = cam.mulRadius(0.9f);
			break;
		case KeyEvent.VK_F:
			cam = cam.mulRadius(1.1f);
			break;
		case KeyEvent.VK_1:
			mode = 0;
			break;
		case KeyEvent.VK_2:
			mode = 1;
			break;
		case KeyEvent.VK_3:
			if(textureMode == 1) textureMode = 0;
			else textureMode++;
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void dispose(GLAutoDrawable glDrawable) {
		GL2GL3 gl = glDrawable.getGL().getGL2GL3();
		gl.glDeleteProgram(shaderProgram);
		gl.glDeleteProgram(shaderProgramPost);
	}

}